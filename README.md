SIO-6014
====================

Exercice #8 / 2013-11-11
---------------------

##Polymer CRM
> Create a single page Web application with HTML5, CSS, local storage and Dart to manage contacts, but this time you must use [Polymer.dart](https://www.dartlang.org/polymer-dart/) for web components.

*Pierre-Olivier Nadeau*
*906 186 811*
